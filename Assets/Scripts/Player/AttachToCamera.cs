﻿using UnityEngine;

public class AttachToCamera : MonoBehaviour {

    private void Update()
    {
        if (transform.position.x <= -8.35f)
            transform.position = new Vector3(-8.35f, transform.position.y, transform.position.z);
        else if (transform.position.x >= 8.35f)
            transform.position = new Vector3(8.35f, transform.position.y, transform.position.z);

        if (transform.position.y <= -4.4f)
            transform.position = new Vector3(transform.position.x, -4.4f, transform.position.z);
        else if (transform.position.y >= 4.4f)
            transform.position = new Vector3(transform.position.x , 4.4f, transform.position.z);
    }

}
