﻿using System.Collections;
using UnityEngine;

public class Shoot : MonoBehaviour {

    #region public
    public GameObject target;
    public GameObject bullet;
    public float timeBetweenEnemy = 2f;
    #endregion

    #region private
    private TeamManager tm;
    private GameObject shooter;
    private bool isShooting;
    private GameObject currentScope;
    private LineRenderer line;
    private GameObject[] enemies;
    #endregion

    //FIND TEAMMANAGER
    void Start()
    {
        tm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<TeamManager>();
        line = GetComponent<LineRenderer>();
        line.startWidth = 0.05f;
        line.positionCount = 2;
    }
    
    private void Update ()
    {
        //CHECK IF PLAYER IS IN SHOOTING FLOOR
        isShooting = GetComponent<Player>().Shooting();
        //IF CAN SHOOT, ENABLE THE SHOOT OR DISABLE THE LINE
        if (isShooting)
        {
            //CROSS OF TARGET
            TargetedEnemy();
            //IF TARGET IS VISIBLE MAKE A LINE TO HIM
            
            if (target != null)
            {
                line.enabled = true;
                DrawLine();
            } else
            {
                line.enabled = false;
            }
            
            //SHOOT
            if (Input.GetButtonDown(GetComponent<PlayerController>().controller + "Fire") && CanSeeTheTarget())
            {
                if (target != null)
                    ShootBullet();
            }  
        } else
        {
            line.enabled = false;
        }
    }

    //00 - GET ENEMY TEAM AND START SHOOTING
    public void FindEnemies ()
    {
        GetTeam();
        target = enemies[0];
        StartCoroutine(WaitForNewEnemie());
    }

    //01 - GET THE ENEMY TEAM
    private void GetTeam()
    {
        for (int i = 0; i < tm.teamA.Length; i++)
        {
            if (tm.teamA[i] == this.gameObject)
            {
                Debug.Log("B");
                enemies = tm.teamB;
            }
        }
        for (int i = 0; i < tm.teamB.Length; i++)
        {
            if (tm.teamB[i] == this.gameObject)
            {
                Debug.Log("A");
                enemies = tm.teamA;
            }
        }
    }

    //02 - WAIT TO GET NEXT ENEMIE
    IEnumerator WaitForNewEnemie ()
    {
        yield return new WaitForSeconds(timeBetweenEnemy);
        GetNextEnemie();
        if (isShooting)
        {
            StartCoroutine(WaitForNewEnemie());
        }
    }

    //03 - GET THE NEXT ENEMY IN THE LIST
    private void GetNextEnemie()
    {
        for (int i = 0; i < enemies.Length; i++)
        {
            if (target == enemies[i])
            {
                if (i < enemies.Length -1)
                {
                    //Debug.Log("NEXTENEMY");
                    target = enemies[i+1];
                    return;

                } else if (enemies.Length > 2)
                {
                    //Debug.Log("FIRSTENEMY");
                    target = enemies[1];
                    return;
                } else
                {
                    //Debug.Log("CEROTENEMY");
                    target = enemies[0];
                }
            }

        }
    }

    //04 - SET THE SCOPE TO THE TARGET
    private void TargetedEnemy()
    {
        if (target != null)
        {
            /*
            Vector3 diffs = target.transform.position - transform.position;
            diffs.Normalize();

            float rot_z = Mathf.Atan2(diffs.z, diffs.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90); //114
            */
            /*
            Vector3 vectorToTarget = target.transform.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            float speed = 100;
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speed);
            */
            target.transform.Find("Scope").gameObject.GetComponent<Scope>().Appear(timeBetweenEnemy);
        }
    }

    //04.1  - RETURN IF THE TARGET IS IN VISION
    private bool CanSeeTheTarget()
    {
        if (target != null)
        {
            //IF IS FALSE, MAKE IT TRUE AND RETURN
            return (!ObjectBetweenTarget());
        } else
        {
            return false;
        }
        
    }

    //04.2 - CHECK FOR OBJECT BETWEEN TARGET
    private bool ObjectBetweenTarget()
    {
        // Look if the target is behind something

        float _distanciaJugador = Vector2.Distance(transform.position, target.transform.position);
        RaycastHit2D[] _toquesRAY = Physics2D.RaycastAll(transform.position, target.transform.position - transform.position, _distanciaJugador);

        //Debug.DrawRay(transform.position, target.transform.position - transform.position, Color.blue); // Show the rect between target and shooter

        foreach (RaycastHit2D hit in _toquesRAY)
        {
            // Ignore Trigger things
            if (hit.transform.tag == "BassFloor" || hit.transform.tag == "TrumpFloor" || hit.transform.tag == "JumpFloor" || hit.transform.tag == "Drummer")
            {
                continue;
            }
            
            // If there is something between target, return false;
            if (hit.transform.tag != "Player")
            {
                //Debug.Log(hit.transform.tag);
                return true;
            }
        }

        // If there is nothing in the middle return true
        return false;

    }

    //04.3 DRAW A LINE BETWEEN TARGET AND SHOOTER
    private void DrawLine()
    {
        //SET FIRST POSITION NEAR PLAYER
        line.SetPosition(0, transform.position);

        //GET THE SECOND POSITION IF THERE IS TARGET
        if (target != null)
        {
            //DISTANCE BETWEEN PLAYER AND TARGET
            float _distance = Vector3.Distance(transform.position, target.transform.position);

            //CHECK FOR OBSTACLES
            Vector3 endPosition = Physics2D.Raycast(new Vector2(transform.position.x + DiffX(), transform.position.y), target.transform.position - transform.position, _distance, 1 << 10).point;

            //IF NOT POINT BETWEEN TARGET, GET THE TARGET POSITION
            if (endPosition == new Vector3(0, 0, 0)) 
            {
                endPosition = target.transform.position;
            }

            //SET THE SECOND POSITION
            line.SetPosition(1, endPosition);
        }
        
    }

    //05 - SHOOT THE BULLET TO THE TARGET
    private void ShootBullet()
    {
        //SPAWN BULLET
        GameObject bull = Instantiate(bullet, new Vector2(transform.position.x + DiffX(), transform.position.y + DiffY()), Quaternion.identity);

        //GIVE THE TARGET TO THE BULLET
        bull.GetComponent<Bullet>().SetTarget(target.transform);
    }

    //RETURN THE X DIFERENCE BETWEEN TARGET
    private float DiffX()
    {
        float result = 0;
        if (target.transform.position.x < transform.position.x)
        {
            result -= 1;
        }
        else
        {
            result += 1;
        }
        return result;
    }

    //RETURN THE Y DIFERENCE BETWEEN TARGET
    private float DiffY()
    {
        float result = 0;
        if (target.transform.position.y < transform.position.y)
        {
            result -= 0.5f;
        }
        else
        {
            result += 0.5f;
        }
        return result;
    }
}
