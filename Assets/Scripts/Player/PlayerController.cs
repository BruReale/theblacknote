﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public string controller = "";
    public GameObject _attack;
    public float attackColdown = 0.5f;
    public float maxSpeed = 800;

    #region private
    private Vector3 direction;
    private Rigidbody2D rg2D;
    private float speed;
    private float attackTime;
    private bool atk;
    #endregion

    void Start ()
    {
        rg2D = GetComponent<Rigidbody2D>();
        attackTime = 0f;
        atk = true;
        //TODO: ADD CONTROLLER ASSIGN
    /*if (Input.GetButton("j1Start"))
    {
        controller = "j1";
    }*/
    }

    private void Update()
    {
        //TIME BEETWEEN ATK
        if (attackTime > 0) { attackTime -= Time.deltaTime; }
        if (attackTime < 0) { attackTime = 0f; }
        //DECREASE SPEED, GRAB DRUMMER
        if (GetComponent<Player>().Grab)
        {
            speed = maxSpeed / 2;
        }
        else
        {
            speed = maxSpeed;
        }
    }

    private void FixedUpdate()
    {
        //THE MOVEMENT OF THE PLAYER
        if (!GetComponent<Player>().Stuned) { 
                rg2D.velocity = new Vector3(Input.GetAxisRaw(controller + "Horizontal") * speed * Time.deltaTime, Input.GetAxisRaw(controller + "Vertical") * speed * Time.deltaTime, 0);
                if (Input.GetButton(controller + "Horizontal") || Input.GetButton(controller + "Vertical"))
                    direction = new Vector2(Input.GetAxisRaw(controller + "Horizontal"), Input.GetAxisRaw(controller + "Vertical"));
        }

        //ATK INPUT
        if (Input.GetButtonDown(controller + "Fire")&&atk&&!GetComponent<Player>().Grab)
        {
            if (attackTime == 0)
            {
                attackTime = attackColdown;
                StartCoroutine(Attack());
            }
        }
    }

    //ATTACK
    IEnumerator Attack()
    {
        GameObject _stick = Instantiate(_attack, transform.position, Quaternion.Euler(direction), transform);
        yield return new WaitForSeconds(attackColdown);
        Destroy(_stick);
    }

    //DISABLED ATK FROM JUMP AND BASS FLOOR
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "JumpFloor" || collision.gameObject.tag == "BassFloor" || collision.gameObject.tag == "TrumpFloor")
        {
            atk = false;
        }
    }

    //ENABLE ATK ON EXIT FROM JUMP AND BASS FLOOR
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "JumpFloor" || collision.gameObject.tag == "BassFloor" || collision.gameObject.tag == "TrumpFloor")
        {
            atk = true;
        }
    }

}
