﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour {

    //STAT OF THE PLAYER
    [System.Serializable]
    public class PlayerStat
    {
        private bool isVulnerable = true;

        //TEMP DEBUG
        private bool grab = false;
        private bool stuned = false;
        private bool isShooting = false;

        public bool Vulnerable
        {
            get { return isVulnerable; }
            set { isVulnerable = value; }
        }

        public bool GetGrab
        {
            get { return grab; }
            set { grab = value; }
        }

        public bool Shooting
        {
            get { return isShooting; }
            set { isShooting = value; }
        }

        public bool Stuned
        {
            get { return stuned; }
            set { stuned = value; }
        }
    }

    public PlayerStat stats = new PlayerStat();
    
    private void Update()
    {
        //MAKE CHARACTER VULNERABLE OUTSIDE OF BASSFLOOR, OR STUNED
        if (GetComponent<PlayerStateController>().GetState() == "Bass" || stats.Stuned)
        {
            stats.Vulnerable = false;
        } else
        {
            stats.Vulnerable = true;
        }
    }

    //CHECK COLLISION WITH BULLET AND STUN
    private void OnTriggerEnter2D(Collider2D collision)
    {

        //IF IS HIT BY BULLET, AND IS VULNERABLE GET STUN
        if (collision.gameObject.tag == "Bullet")
        {
            Debug.Log("HIT");
            if (stats.Vulnerable)
            {
                Debug.Log("STUNNED");
                Destroy(collision.gameObject);
                StartCoroutine(Stun());
            } else
            {
                Destroy(collision.gameObject);
            }
        }

        //ACTIVE SHOOTING IN TRUMP FLOOR
        if (collision.gameObject.tag == "TrumpFloor")
        {
            stats.Shooting = true;
            GetComponent<Shoot>().FindEnemies();
        }

        //IF IS HIT BY MELEE ATTACK STUN
        if (collision.gameObject.tag == "Stick")
        {
            StartCoroutine(Stun());
        }
    }

    //CHECK IF PLAYER LEFT TRUMPFLOOR
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "TrumpFloor")
        {
            stats.Shooting = false;
        }
    }

    //STUN THE PLAYER FOR X TIME
    private IEnumerator Stun()
    {
        Debug.Log("STUN");
        stats.Stuned = true;
        yield return new WaitForSeconds(2);
        Debug.Log("UNSTUN");
        stats.Stuned = false;
    }

    #region GETSET
    //RETURN IF THE PLAYER GRAB THE DRUMMER
    public bool Grab
    {
        get { return stats.GetGrab; }
        set { stats.GetGrab = value; }
    }

    //CHEKC IF THE PLAYER IS SHOOTING
    public bool Shooting()
    {
        return stats.Shooting;
    }

    //GET SET IF STUN
    public bool Stuned
    {
        get { return stats.Stuned; }
        set { stats.Stuned = value; }
    }
    #endregion

    /*
    //WAIT FOR RESPAWN
    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(stats.Time);
        SpawnPlayer();
    }

    //RESPAWN THE PLAYER TO HIS POINT SPAWN
    private void SpawnPlayer()
    {
        transform.position = spawnPoint.position;
    }
    */
}
