﻿using System.Collections;
using UnityEngine;

public class PlayerStateController : MonoBehaviour {

    #region state
    private enum BandState { Singer, Bass, Trumpet, Stuned, Jump };
    private BandState state = BandState.Singer;
    #endregion

    #region private
    private Animator anim;
    private GameObject floorType;
    #endregion

    //DEBUG
    public string _state;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        //DEBUG
        _state = state.ToString();

        //CALL THE MACHINE
        StateAction();

        //STATE MACHINE CONDITIONS
        if (floorType != null && !GetComponent<Player>().Grab && !GetComponent<Player>().Stuned)
        {
            if (floorType.tag == "BassFloor")
            {
                state = BandState.Bass;
            } else
            {
                state = BandState.Trumpet;
            }
        } else
        {
            if (GetComponent<Player>().Stuned)
            {
                state = BandState.Stuned;
            } else
            {
                state = BandState.Singer;
            }
        }
    }

    //STATE MACHINE
    private void StateAction()
    {
        switch (state)
        {
            case BandState.Singer:
                anim.SetBool("Bass", false);
                anim.SetBool("Trump", false);
                anim.SetBool("Stuned", false);
                break;
            case BandState.Bass:
                //anim.SetTrigger("Bass");
                anim.SetBool("Bass", true);
                break;
            case BandState.Trumpet:
                anim.SetBool("Trump", true);
                break;
            case BandState.Stuned:
                anim.SetBool("Stuned", true);
                break;
            case BandState.Jump:
                //SOMETHING HERE?
                break;
            default: break;
        }
    }
    
    //CHECK COLLISION WITH SPECIAL FLOOR
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "BassFloor" || other.gameObject.tag == "TrumpFloor")
        {
            floorType = other.gameObject;
        }
    }

    //CHECK IF LEFT A FLOOR
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BassFloor" || collision.gameObject.tag == "TrumpFloor")
        {
            floorType = null;
        }
    }

    //GET THE CURRENT STATE OF THE PLAYER
    public string GetState()
    {
        string _state = state.ToString();
        return _state;
    }

}
