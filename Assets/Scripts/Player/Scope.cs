﻿using System.Collections;
using UnityEngine;

public class Scope : MonoBehaviour {

    //GET THE TIME TO WAIT AND CALL THE ROUTINE
    public void Appear(float time)
    {
        this.gameObject.SetActive(true);
        StartCoroutine(Show(time));
    }

    //ACTIVE CROSS, AND WAIT FOR DISABLE
    IEnumerator Show(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        this.gameObject.SetActive(false);
    }
}
