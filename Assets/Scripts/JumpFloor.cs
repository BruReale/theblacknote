﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpFloor : MonoBehaviour {

    //TWIN JUMP
    public Transform JumpB;
    public bool isPlayer;

    //TELEPORT ON ENTER
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //COLLISION WITH PLAYER AND DONT GOT DRUMMER CAN TELEPORT
        if (collision.gameObject.tag == "Player" && !collision.gameObject.GetComponent<Player>().Grab)
        {
            string controller = collision.gameObject.GetComponent<PlayerController>().controller;
            if (Input.GetButtonDown(controller + "Fire"))
            {
                collision.gameObject.transform.position = JumpB.position;
            }
        }
    }

    //TELEPORT ON STAY
    private void OnTriggerStay2D(Collider2D collision)
    {
        //COLLISION WITH PLAYER AND DONT GOT DRUMMER CAN TELEPORT
        if (collision.gameObject.tag == "Player" && !collision.gameObject.GetComponent<Player>().Grab)
        {
            isPlayer = true;
            string controller = collision.gameObject.GetComponent<PlayerController>().controller;
            Debug.Log(controller);
            if (Input.GetButtonDown(controller + "Fire"))
            {
                Debug.Log("Teleport");
                collision.gameObject.transform.position = JumpB.position;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isPlayer = false;
        }
    }
    
}
