﻿using UnityEngine;

public class Drummer : MonoBehaviour {

    public GameObject characterGrab;
    public bool isGrab;

    private void Update()
    {
        //CHECK WHO GRAB, AND CHANGE POSITION
        if (characterGrab == null)
        {
            isGrab = false;
        } else
        {
            isGrab = true;
            transform.position = characterGrab.transform.position;
            characterGrab.GetComponent<Player>().Grab = true;
        }
    }

    //CHECK FOR GAME FINISH-------------------------------------
    //TODO: CHANGE THIS IN BASEDRUMMER
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BaseDrum")
        {
            Destroy(this);
        }
    }

    //GRAB THE DRUMMER WHO PRESS HIS ACTION KEY
    private void OnTriggerStay2D(Collider2D collision)
    {
        //GRAB THE OBJ
            if (collision.gameObject.tag == "Player")
            {
                GameObject obj = collision.gameObject;
                string controller = obj.GetComponent<PlayerController>().controller;
                if (Input.GetButtonDown(controller + "Fire"))
                {
                    if (!isGrab)
                    {
                        characterGrab = obj;
                        isGrab = true;
                    } else
                    {
                        characterGrab.GetComponent<Player>().Grab = false;
                        characterGrab = null;
                        isGrab = false;
                    }
                }
            }
    }

    //CHECK IF SOMEONE CATCH THE OBJECT
    public bool GetGrab ()
    {
        return isGrab;
    }
}
