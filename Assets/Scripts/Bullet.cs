﻿using UnityEngine;

public class Bullet : MonoBehaviour {

    #region var
    private Transform target;
    private Vector3 pos;
    private float sp = 500;
    #endregion

    private void FixedUpdate ()
    {
        if (transform.position == pos)
        {
            Destroy(this.gameObject);
        }
        //MOVE THE BULLET
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, pos, sp * Time.deltaTime);
        } else
        {
            Destroy(this);
        }
    }

    //GET THE TARGET TO MOVE
	public void SetTarget (Transform _target)
    {
        //SET THE POS TO MOVE
        target = _target;
        pos = _target.position;
        /////////////////////////

        //ROTATION
        
        Vector3 diff = _target.position - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.z, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 114);

        /*
        Quaternion q = Quaternion.AngleAxis(rot_z, this.transform.up);
        const float sp = 100;
        transform.rotation = Quaternion.Lerp(transform.rotation, q, Time.deltaTime * sp);
        */




        ///////////////// ROTATION TEST ///////////////////////
        /*
        Vector2 direction = _target.position - transform.position;
        Quaternion _facing;
        _facing = transform.rotation;
        Quaternion rot = Quaternion.LookRotation(direction);
        rot *= _facing;
        transform.rotation = rot;
        */
        ///////////////////////////////////
        //SET THE ANGLE OF THE BULLET
        /*
        Quaternion rotation = new Quaternion();
        rotation.SetFromToRotation(transform.position + pos, target.position);
        transform.localRotation = rotation * target.rotation;
        */
        //transform.position = Quaternion.SetLookRotation(target.position, up); //Quaternion.Slerp(transform.rotation, target.rotation, 1);
    }

}
