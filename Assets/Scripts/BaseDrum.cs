﻿using UnityEngine;
using UnityEngine.UI;

public class BaseDrum : MonoBehaviour {

    public GameObject menu;
    public Text winner;
    public string team;

    private void Awake()
    {
        menu = GameObject.FindGameObjectWithTag("Menu");
        winner = GameObject.FindGameObjectWithTag("Winner").GetComponent<Text>();
    }

    private void Start()
    {
        menu.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Drummer")
        {
            winner.text = team;
            menu.SetActive(true);
            Destroy(collision.gameObject);
            Time.timeScale = 0f;
        }
    }

}
