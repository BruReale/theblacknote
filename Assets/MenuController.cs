﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

	public void PlayAgain()
    {
        Scene _sceneBuildIndex = SceneManager.GetActiveScene();
        SceneManager.LoadScene(_sceneBuildIndex.buildIndex);
        Time.timeScale = 1f;
    }

    public void Exit()
    {
        Application.Quit();
    }
}
