﻿using UnityEngine;

public class TeamManager : MonoBehaviour {

    public int maxPerTeam = 2;

    public GameObject[] players;

    public GameObject[] teamA = new GameObject[2];
    public GameObject[] teamB = new GameObject[2];

    public Transform[] spawnPoint = new Transform[2];

    public GameObject player;

    private void Start()
    {
        //MakeTeams();
    }

    
    private void MakeTeams()
    {
        teamA = new GameObject[maxPerTeam];
        teamB = new GameObject[maxPerTeam];

        teamA[0] = new GameObject();

        for (int i = 1; i < teamA.Length; i++)
        {
            //teamA[i] = Instantiate(player, transform.position, Quaternion.identity, transform);
        }
    }

    public GameObject[] GetTeam()
    {
        return teamA;
    }
}
